// -*- mode: c; -*- //

// Amber's squid.  In space.

var camera, scene, renderer, loader;
var things = [];
var squid, cloud;

var farAway = -1000, upClose = 1000;

function init()
{
    camera = new THREE.PerspectiveCamera(
                                         120, // FOV
                                         window.innerWidth / window.innerHeight, // aspect 
                                         1,   // near
                                         4000 // far
                                         );
    camera.position.z = upClose;
    
    scene = new THREE.Scene();
    scene.add(camera);

    renderer = new THREE.WebGLRenderer({ alpha: true });
    windowResized();

    document.body.appendChild( renderer.domElement );

    loader = new THREE.TextureLoader();
    loader.load('squid-16c.png', function(t) { squid = t; });
    loader.load('cloud-b-1.png', function(t) { cloud = t; });

    window.addEventListener( 'resize', windowResized, false );

    update();
}
init();


function update() {
    requestAnimationFrame(update);

    if (!squid || !cloud)
        return;

    if (things.length < 1)
        makeThings();

    updateThings();
    renderer.render( scene, camera );
}

function randomRange(x)
{
    return (Math.random() * x) - (x/2);
}

function makeThings() { 
    var thing, material; 
    var index=0;

    for (var z = farAway; z < upClose; z += 20) {
        var doCloud = (index % 10) != 0;

        material = new THREE.SpriteMaterial({
            map: doCloud ? cloud : squid,
            transparent: true
        });
        thing = new THREE.Sprite(material);

        // give it a random x and y position between -500 and 500
        thing.position.x = randomRange(1000);
        thing.position.y = randomRange(1000);
        thing.position.z = z;
        
        thing.scale.x = thing.scale.y = doCloud ? 50 : 100;
        
        scene.add(thing);

        things.push(thing);
        index++;
    }

    // get rid of loading message
    var e = document.getElementById('loadingMsg');
    if (e) document.body.removeChild(e);
}

function updateThings() { 

    for (var i = 0; i < things.length; i++) {
        var t = things[i];
        t.position.z +=  15;
        if (t.position.z > upClose) {
            t.position.x = randomRange(1000);
            t.position.y = randomRange(1000);
            t.position.z = farAway;
        }
        
    }
    
}

function windowResized() {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
}
